#!/bin/sh

usermod -u 1000 www-data
groupmod -g 1000 www-data

if [[ "$@" = "php-fpm" ]]; then
  $@
else
  gosu www-data $@
fi